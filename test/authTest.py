import unittest
from test.selenium import webdriver
from test.pages import *
from test.locators import *



class TestAuthPages(unittest.TestCase):

    #set up webdriver options
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\selenium\chromedriver_win32\chromedriver.exe')
        self.driver.maximize_window()
        #self.driver.get("https://staging.rungrinh.vn/")
        #self.driver.get("https://rungrinh.vn/")
        self.driver.get ("https://dev.rungrinh.vn/")

    #test - wait for main site page loading
    def test_page_load(self):
        page = MainPage(self.driver)
        page.wait_for_element_is_visible(MainPageLocators.LOGO,5)
        self.assertTrue(page.check_page_loaded())

    #test - click SignUp button on main page
    def test_sign_up_button(self):
        page = MainPage(self.driver)
        signUpPage = page.click_sign_in_button()
        signUpPage.wait_for_element_is_visible(LoginPageLocators.SIGNUP_PAGE,5)
        print(signUpPage.get_url())
        self.assertIn("/login", signUpPage.get_url())

    #test - click Join button on main page
    def test_join_button(self):
        page = MainPage(self.driver)
        joinPage = page.click_join_button()
        joinPage.wait_for_element_is_visible(JoinPageLocators.JOIN_PAGE,5)
        print (joinPage.get_url ())
        self.assertIn("/join", joinPage.get_url())

    #test - login with correct user data
    def test_login(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login()
        logineduserpage.wait_for_element_is_visible(LoginedUserPageLocators.USERNAME,5)
        self.assertTrue(logineduserpage.check_user_logined())

    #test login with incorrect user data
    def test_loginError(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login_incorrect()
        logineduserpage.wait_for_element_is_visible(LoginedUserPageLocators.LOGIN_ERROR,5)
        self.assertTrue(logineduserpage.check_login_error_presented())

    #test  - open Forgot password window from SignIn page
    def test_forgotpassword(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        self.assertTrue(signUpPage.forgot_password())

    #test - open Join page by link from SignIn page
    def test_gotojoin(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        joinPage = signUpPage.go_to_join()
        joinPage.wait_for_element_is_visible(JoinPageLocators.JOIN_PAGE,5)
        self.assertIn("/join", joinPage.get_url())

    #test - close SignIn popup via X button
    def test_closesigninX(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        mainPage = signUpPage.close_signin()
        mainPage.wait_for_element_is_visible(MainPageLocators.LOGO,5)
        self.assertTrue(mainPage.check_page_loaded())

    #test - close SignIn popup by clicking outside it
    def test_closesignin(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        mainPage = self.driver.switch_to.default_content()
        mainPage  = signUpPage.click_logo()
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue(mainPage.check_page_loaded())

    #test - fb login popup open
    def test_fbopen(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        signUpPage.signin_fb ()
        self.assertTrue (signUpPage.check_fb())

    #test - fb signup login with correct data
    def test_signinfacebook(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        facebookPopup = signUpPage.signin_fb()
        self.driver.switch_to.window (self.driver.window_handles[1])
        facebookPopup.wait_for_element_is_visible(FacebookPopupLocators.FB_WINDOW,5)
        loginedPage = facebookPopup.login_fb_correct()
        loginedPage.wait_for_element_is_visible(LoginedUserPageLocators.USERNAME,5)
        self.assertTrue (loginedPage.check_user_logined ())

    #test - fb signup login with incorrect data
    def test_signinerrorfacebook(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        facebookPopup = signUpPage.signin_fb ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        facebookPopup.wait_for_element_is_visible (FacebookPopupLocators.FB_WINDOW, 5)
        facebookPopup.login_fb_incorrect ()
        self.assertTrue(facebookPopup.check_fb())


    #test - close fb popup by Not now button
    def test_closefb(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        facebookPopup = signUpPage.signin_fb ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        facebookPopup.wait_for_element_is_visible (FacebookPopupLocators.FB_WINDOW, 5)
        signUpPage = facebookPopup.close_fb()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        self.assertIn ("/login", signUpPage.get_url ())

    #test - open google signin popup
    def test_googleopen(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        googlePopup = signUpPage.signin_google()
        self.driver.switch_to.window (self.driver.window_handles[1])
        googlePopup.wait_for_element_is_visible (GooglePopupLocators.GOOGLE_WINDOW, 5)
        self.assertTrue (signUpPage.check_google ())

    #test - google correct sign in
    def test_signingoogle(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        googlePopup = signUpPage.signin_google ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        googlePopup.wait_for_element_is_visible (GooglePopupLocators.GOOGLE_WINDOW, 5)
        loginedPage = googlePopup.login_go_correct ()
        loginedPage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        self.assertTrue (loginedPage.check_user_logined ())

    #test - sign in with google incorrect, got error
    def test_signingoogleincorrect(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        googlePopup = signUpPage.signin_google ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        googlePopup.wait_for_element_is_visible (GooglePopupLocators.GOOGLE_WINDOW, 5)
        resultPage = googlePopup.login_go_incorrect ()
        self.assertTrue(googlePopup.check_glogin_error_presented())


    #test - google popup close by clicking outside it
    def test_closegoogle(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        googlePopup = signUpPage.signin_google ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        googlePopup.wait_for_element_is_visible (GooglePopupLocators.GOOGLE_WINDOW, 5)
        signUpPage = googlePopup.close_google ()
        signUpPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue (signUpPage.check_page_loaded ())

    #test - open fb popup via Join page
    def test_joinfacebook(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        joinPage = signUpPage.go_to_join ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        signUpPage.join_fb()
        self.assertTrue (signUpPage.check_fb ())

    #test - correct fb login via Join page
    def test_joinloginfacebook(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        facebookPopup = joinPage.join_fb ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        facebookPopup.wait_for_element_is_visible(FacebookPopupLocators.FB_WINDOW,5)
        loginedPage = facebookPopup.login_fb_correct()
        self.driver.switch_to.window (self.driver.window_handles[0])
        loginedPage.wait_for_element_is_visible(LoginedUserPageLocators.USERNAME,5)
        self.assertTrue (loginedPage.check_user_logined ())

    #test - incorrect fb login via Join page, got error
    def test_joinfbincorrect(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        facebookPopup = joinPage.join_fb ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        facebookPopup.wait_for_element_is_visible (FacebookPopupLocators.FB_WINDOW, 5)
        loginedPage = facebookPopup.login_fb_incorrect()
        self.assertTrue(facebookPopup.check_fb_error_presented())

    #test - fb popup close from Join page
    def test_joinclosefb(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        facebookPopup = joinPage.join_fb ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        facebookPopup.wait_for_element_is_visible (FacebookPopupLocators.FB_WINDOW, 5)
        joinPage = facebookPopup.close_fb()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        self.assertIn ("/join", joinPage.get_url ())

    #test - Google popup from Join page open
    def test_joingoogle(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        joinPage = signUpPage.go_to_join ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        signUpPage.join_google()
        self.assertTrue (signUpPage.check_google())


    #test - Login via Google from Join page
    def test_joinlogingoogle(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        googlePopup = joinPage.join_google()
        self.driver.switch_to.window (self.driver.window_handles[1])
        googlePopup.wait_for_element_is_visible (GooglePopupLocators.GOOGLE_WINDOW, 5)
        loginedPage = googlePopup.login_go_correct()
        self.driver.switch_to.window (self.driver.window_handles[0])
        loginedPage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        self.assertTrue (loginedPage.check_user_logined ())

    #test - login via google from Join page, got error (wrong email\pass)
    def test_joingoogleerror(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        googlePopup = joinPage.join_google()
        self.driver.switch_to.window (self.driver.window_handles[1])
        googlePopup.wait_for_element_is_visible (GooglePopupLocators.GOOGLE_WINDOW, 5)
        loginedPage = googlePopup.login_go_incorrect()
        self.assertTrue (googlePopup.check_glogin_error_presented())

    #test - close opened Google login popup from Join page
    def test_joingoogleclose(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        googlePopup = joinPage.join_google ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        googlePopup.wait_for_element_is_visible (GooglePopupLocators.GOOGLE_WINDOW, 5)
        signUpPage = googlePopup.close_google ()
        signUpPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue (signUpPage.check_page_loaded ())

    #test - join with already taken email, got error
    def test_jointakenemail(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        logineduserpage = joinPage.jlogin_taken()
        logineduserpage.wait_for_element_is_visible (JoinPageLocators.EMAIL_TAKEN, 5)
        self.assertTrue (logineduserpage.check_jlogin_error_presented ())

    #test - join with empty login\pass, got error
    def test_joinempty(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        logineduserpage = joinPage.jlogin_empty ()
        logineduserpage.wait_for_element_is_visible (JoinPageLocators.ERROR_EMPTY, 5)
        self.assertTrue (logineduserpage.check_jlogin_errorempty_presented ())

    #test - go to sign in popup via join page (Member Sign in button)
    def test_jointosignin(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        signUpPage = joinPage.goto_signin()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        self.assertTrue (signUpPage.check_page_loaded ())

    #teset - close join popup, goes to main page
    def test_closejoinpage(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        mainPage = joinPage.close_join()
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue (mainPage.check_page_loaded ())

    #test - close join popup by clicking uotside of it
    def test_clickoutsidjoin(self):
        page = MainPage (self.driver)
        joinPage = page.click_join_button ()
        joinPage.wait_for_element_is_visible (JoinPageLocators.JOIN_PAGE, 5)
        mainPage = joinPage.click_logo()
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue (mainPage.check_page_loaded ())



    #close driver after the test
    def tearDown(self):
        self.driver.close()

