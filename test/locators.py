from test.selenium.webdriver.common.by import By

# for maintainability we can seperate web objects by page name

#locators for main site page
class MainPageLocators(object):
    URL = "https://rungrinh.vn/"
    LOGO = (By.CSS_SELECTOR,'#main-wrapper > div.header > a > span.logo__img')
    SIGNUP = (By.CSS_SELECTOR,'#main-wrapper > div.header > div:nth-child(2) > div > a:nth-child(1) > div > button')
    JOIN = (By.CSS_SELECTOR,'#main-wrapper > div.header > div:nth-child(2) > div > a:nth-child(2) > div > button ')
    SEARCH = (By.CSS_SELECTOR,'div.search > label')
    SEARCH_INPUT = (By.CSS_SELECTOR, 'div.search > input')
    RESULTS = (By.CSS_SELECTOR, '[role="presentation"] [role="menu"] .search__item')
    LANG_CHOOSE = (By.CSS_SELECTOR,'div.select.select_green.lang-switcher > div > div')
    LANG_LIST = (By.CSS_SELECTOR,'[role="presentation"] [role="menu"] .lang-switcher__item')
    FACEBOOK = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__distribution > div > span.footer__social.footer__social_facebook')
    TWITTER = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__distribution > div > span.footer__social.footer__social_twitter')
    GOOGLE = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__distribution > div > span.footer__social.footer__social_google')
    FOR_ADV = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__distribution > a > div > button > div > div > span')
    HOW_LINK = (By.CSS_SELECTOR,'#main-wrapper > div.header > nav > a:nth-child(2)')
    ABOUT_ADD = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__links > nav > ul > li:nth-child(2) > a')
    TERMS = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__links > nav > ul > li:nth-child(3) > a')
    PRIVACY = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__links > nav > ul > li:nth-child(4) > a')
    HELP = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__links > nav > ul > li:nth-child(5) > a')
    ALI = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__stores > ul > a > li')
    QATAR = (By.CSS_SELECTOR,'#main-wrapper > footer > div > div.footer__coupons > ul > a > li')
    ADD_EXT = (By.CSS_SELECTOR,'#main-wrapper > div:nth-child(5) > div > div > a > div > button > div > div > span')
    CLOSE_EXT = (By.CSS_SELECTOR,'#main-wrapper > div:nth-child(5) > span')
    EXT_NOTIFICATION_OPEN  = (By.CSS_SELECTOR,'.distribution-toolbar__open')
    ALL_STORES = (By.CSS_SELECTOR,'#main-wrapper > div.header > nav > div > a')
    OFFERS = (By.CSS_SELECTOR,'.nesting-list')
    OFFERS_CAT = (By.CSS_SELECTOR,'.nesting-list > ul> li')
    OFFERS_SUB = (By.CSS_SELECTOR,'#main-wrapper > div.header > nav > div > div > div > div > div > ul > li:nth-child(1)')
    OFFER_GO = (By.CSS_SELECTOR, '.nesting-list__cashback')
    SUPPORT = (By.CSS_SELECTOR,'#main-wrapper > div.header > nav > a:nth-child(3)')
    BANNER_NOLOGIN = (By.CSS_SELECTOR,'#main-wrapper > div.container.landing > div.banner.banner_top')
    JOIN_NOLOGIN = (By.CSS_SELECTOR,'#main-wrapper > div.container.landing > div.banner.banner_top > div.banner__side.banner__side_right > div > div > form')
    GET_THE_BUTTON = (By.CSS_SELECTOR,'#main-wrapper > div.container.landing > div.how-to-use > div.how-to-use__block.how-to-use_block_white.how-to-use__block_with-btn > div > button > div > div > span')
    BDO_1 = (By.CSS_SELECTOR,'#main-wrapper > div.container.landing > div.landing__offers > div.content > div.top-offer > div > div:nth-child(1) > div > div > div > div > div > div > button > div > div > span')
    SEE_ALL = (By.CSS_SELECTOR,'#main-wrapper > div.container.landing > div.landing__offers > div.landing__button-wrapper > a > div > button > div > div > span')
    BOTTOMBANNER_NOLOGIN = (By.CSS_SELECTOR,'#main-wrapper > div.container.landing > div.banner.banner_bottom')
    BOTTOM_JOIN = (By.CSS_SELECTOR,'.banner__join-footer')
    JOINB_EMAIL = (By.ID,'join-banner_bottom-email')
    JOINB_PASS = (By.ID,'join-banner_bottom-password')
    JOINB_SUBMIT = (By.CSS_SELECTOR,'#main-wrapper > div.container.landing > div.banner.banner_bottom > div.banner__side.banner__side_right > div > div > form > div:nth-child(6) > button')
    SHOPNOW_BANNER = (By.CSS_SELECTOR,'#main-wrapper > div.container > div.slick-initialized.slick-slider.carousel > div > div > div.slick-slide.slick-active.slick-center.slick-cloned.slick-current.carousel__item > div > div > button > div > div > span')



class AllStoresLocators(object):
    BANNER = (By.CSS_SELECTOR,'.banner__form')


#locators for SignIn popup
class LoginPageLocators(object):
  SIGNUP_PAGE = (By.CLASS_NAME, 'login')
  EMAIL         = (By.ID, 'sign-in-email')
  PASSWORD      = (By.ID, 'sign-in-password')
  SUBMIT        = (By.CSS_SELECTOR, 'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div > form > div:nth-child(3) > button')
  FORGOT_PASSWORD = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div > form > a')
  FP_WINDOW = (By.CSS_SELECTOR,'.forgot-password')
  FORGOT_PASSWORD_EMAIL = (By.ID,'forgot-password-email')
  RESTORE_PASS_BUTTON = (By.CSS_SELECTOR,'body > div:nth-child(7) > div > div:nth-child(1) > div > div > div > div > div:nth-child(3) > button > div > div')
  OK_NOTIFICATION = (By.CSS_SELECTOR,'body > div:nth-child(7) > div > div:nth-child(1) > div > div > header > span')
  GO_TOJOIN = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > footer > a')
  X_CLOSE = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > header > a')
  LOGO = (By.CSS_SELECTOR, '#main-wrapper > div.header > a > span.logo__img')
  FACEBOOK_BUTTON = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(1) > button > div > div > span')
  FB_WINDOW = (By.ID, 'facebook')
  GOOGLE_BUTTON = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div > div:nth-child(2) > button > div > div > span')
  GOOGLE_WINDOW = (By.CLASS_NAME, 'CMgTXc')
  JOIN_FACEBOOK_BUTTON = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div:nth-child(2) > form > div:nth-child(1) > button > div > div > span')
  JOIN_GOOGLE_BUTTON = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div:nth-child(2) > form > div:nth-child(2) > button > div > div > span')

#locators for Join popup
class JoinPageLocators(object):
  LOGO = (By.CSS_SELECTOR, '#main-wrapper > div.header > a > span.logo__img')
  GOTO_SIGNIN = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > header > div.join__top > a')
  CLOSE_JOIN = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > footer > p > a')
  JOIN_PAGE = (By.CLASS_NAME,'join')
  EMAIL         = (By.ID, 'join-email')
  PASSWORD      = (By.ID, 'join-password')
  SUBMIT        = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div:nth-child(2) > form > div:nth-child(6) > button > div > div > span')
  ERROR_MESSAGE = (By.PARTIAL_LINK_TEXT, 'This field is required')
  JOIN_FACEBOOK_BUTTON = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div:nth-child(2) > form > div:nth-child(1) > button > div > div > span')
  JOIN_GOOGLE_BUTTON = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div:nth-child(2) > form > div:nth-child(2) > button > div > div > span')
  EMAIL_TAKEN = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div:nth-child(2) > form > div.join__fields > div:nth-child(1) > div:nth-child(4)')
  ERROR_EMPTY = (By.CSS_SELECTOR,'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div:nth-child(2) > form > div.join__fields > div:nth-child(1) > div:nth-child(4)')

#locators for main page for logined user
class LoginedUserPageLocators(object):

    USERNAME = (By.CLASS_NAME, 'user-info__name')
    ALL_STORES = (By.CSS_SELECTOR,'#main-wrapper > div.header > nav > div > a > span.line-menu__label > span')
    SPECIAL_DEAL = (By.CSS_SELECTOR,'#main-wrapper > div.container > div.content > div.special-deal > h1')
    SPECIAL_DEAL_CLICK = (By.CSS_SELECTOR,'#main-wrapper > div.container > div.content > div.special-deal > div > div:nth-child(1) > div > div > div:nth-child(3) > div > div > button > div > div > span')
    TOP_OFFER = (By.CSS_SELECTOR,'#main-wrapper > div.container > div.content > div.top-stores > a:nth-child(1) > div > div > span')
    BD_OFFER = (By.CSS_SELECTOR,'#main-wrapper > div.container > div.content > div.top-offer > h1')
    BD_OFFER_CLICK = (By.CSS_SELECTOR,'#main-wrapper > div.container > div.content > div.top-offer > div > div:nth-child(1) > div > div > div > div > div > div > button > div > div > span')
    USER_CASHBACK = (By.CSS_SELECTOR,'#main-wrapper > div.header > div:nth-child(2) > a > div > span.user-info__cashback')
    LOGIN_ERROR = (By.CSS_SELECTOR, 'body > div:nth-child(5) > div > div:nth-child(1) > div > div > div > div > div > form > div.login__fields > div:nth-child(1) > div:nth-child(4)')
    SHOPNOW_BANNER = (By.CSS_SELECTOR,'#main-wrapper > div.container > div.slick-initialized.slick-slider.carousel > div > div > div.slick-slide.slick-active.slick-center.slick-cloned.slick-current.carousel__item > div > div > button > div > div > span')

class FacebookPopupLocators(object):
    FB_WINDOW = (By.ID,'facebook')
    EMAIl_FB = (By.ID,'email')
    PASS_FB = (By.ID,'pass')
    LOGIN_FB = (By.ID,'loginbutton')
    CLOSE = (By.CSS_SELECTOR,'._2iem')
    ERROR = (By.CSS_SELECTOR,'#globalContainer > div.uiContextualLayerPositioner._572t.uiLayer > div')


class GooglePopupLocators(object):
    GOOGLE_WINDOW = (By.CLASS_NAME,'CMgTXc')
    CHANGE_ACC = (By.CSS_SELECTOR,'.vdE7Oc f3GIQ')
    EMAIL_GO = (By.ID,'identifierId')
    PASS = (By.ID,'password')
    PASS_GO = (By.CSS_SELECTOR,'#password > div.aCsJod.oJeWuf > div > div.Xb9hP > input')
    LOGO = (By.CSS_SELECTOR, '#main-wrapper > div.header > a > span.logo__img')
    ERROR = (By.CSS_SELECTOR,'#view_container > div > div > div.pwWryf.bxPAYd > div > div.WEQkZc > div > form > content > div.cDSmF > div > div.LXRPh > div.dEOOab.RxsGPe')


