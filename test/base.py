from test.selenium import webdriver
from test.selenium.webdriver.common.by import By
from test.selenium.webdriver.common.action_chains import ActionChains
from test.selenium.common.exceptions import TimeoutException
from test.selenium.webdriver.common.by import By
import test.selenium.webdriver.support.expected_conditions as EC
import test.selenium.webdriver.support.ui as ui


# this Base class is serving basic attributes for every single page inherited from Page class


from test.selenium.common.exceptions import NoSuchElementException


class Page (object):
    url = None

    def __init__(self, driver):
        self.driver = driver

    def navigate(self):
        self.driver.get (self.url)

    def find_element(self, *locator):
        return self.driver.find_element (*locator)

    def open(self, url):
        url = self.base_url + url
        self.driver.get (url)

    def get_title(self):
        return self.driver.title

    def get_url(self):
        return self.driver.current_url

    def hover(self, *locator):
        element = self.find_element (*locator)
        hover = ActionChains (self.driver).move_to_element (element)
        hover.perform ()

    def isElementPresent(self, locator):
        try:
            self.driver.find_element_by_css(locator)
        except NoSuchElementException:
            print ('No such element present')
            return False
        return True

    # return True if element is visible within 2 seconds, otherwise False
    def wait_for_element_is_visible(self, locator, timeout=2):
        try:
            ui.WebDriverWait(self.driver, timeout).until(EC.visibility_of_element_located(locator))
            return True
        except TimeoutException:
            return False

    # return True if element is not visible within 2 seconds, otherwise False
    def wait_for_element_is_not_visible(self, locator, timeout=2):
        try:
            ui.WebDriverWait(self.driver, timeout).until(EC.invisibility_of_element_located(locator))
            return True
        except TimeoutException:
            return False




