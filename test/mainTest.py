
import unittest
from test.selenium import webdriver
from test.selenium.webdriver.common.keys import Keys
from test.pages import *
from test.locators import *


class TestMainPages(unittest.TestCase):

    #set up webdriver options
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\selenium\chromedriver_win32\chromedriver.exe')
        self.driver.maximize_window()
        #self.driver.get("https://staging.rungrinh.vn/")
        #self.driver.get("https://rungrinh.vn/")
        self.driver.get ("https://dev.rungrinh.vn/")


    #test - redirect by clicking logo in header. leads to main page
    def test_logoredirect(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue(mainPage.check_page_loaded())

    #test - search with correct request 'ali'
    def test_searchcorrect(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        result = mainPage.enter_search('ali')
        self.assertIn('Aliexpress', result)

    #test - search with space in request
    def test_searchempty(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        mainPage.enter_searchempty(' ')
        self.assertTrue(mainPage.wait_for_element_is_visible(MainPageLocators.RESULTS,5))

    #test - search with no results for request 'tester'
    def test_search_noresults(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        mainPage.enter_searchempty('tester')
        self.assertTrue (mainPage.wait_for_element_is_not_visible (MainPageLocators.RESULTS, 5))

    #test - click on search result for ali - Aliexpress offer page opens
    def test_searchclick(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        mainPage.enter_search('ali')
        mainPage.click_search()
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertIn ("/offers", mainPage.get_url ())

    #test - open dropdown for language choose
    def test_langlistopen(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        mainPage.lang_choose()
        self.assertTrue(mainPage.check_langlistdisplayed())

    #test - change language from default vi to en, via lang dropdown
    def test_langchange(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        mainPage.lang_choose ()
        element1 = mainPage.lang_change()
        self.assertIn ('EN', element1)

    #test - go to RungRinh facebook page from footer - follow us block
    def test_followfacebook(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        fb = mainPage.follow_facebook()
        self.assertIn('facebook', fb.get_url())

    #test - click Twitter icon in footer
    def test_followtwitter(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        tw = mainPage.follow_twitter()
        self.assertIn('twitter', tw.get_url())

    #test - click Google icon in footer
    def test_followgoogle(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        go = mainPage.follow_google()
        self.assertIn('google', go.get_url())

    #test - click For advertisers link in footer
    def test_goforadd(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        add = mainPage.go_foradd ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn ('cityads.com/main/advertiser', add.get_url ())

    #test - click How works link in footer
    def test_howworksclick(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        hw = mainPage.go_howworks ()
        self.assertIn ('/support', hw.get_url ())

    #test - click About adv/ link in footer
    def test_aboutadd(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        add = mainPage.go_aboutadd ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn ('cityads.com/main/advertiser', add.get_url ())

    #test - click Terms&Conditions link in footer
    def test_openterms(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        term = mainPage.go_terms ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn ('/terms-and-conditions', term.get_url ())

    #test - click Privacy link in footer
    def test_openprivacy(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        pr = mainPage.go_privacy ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn ('/privacy-policy', pr.get_url ())

    #test - click Help link in footer
    def test_openhelp(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        h = mainPage.go_help ()
        self.assertIn ('/support', h.get_url ())

    #test - click Aliexpress offer in footer
    def test_goali(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        offer = mainPage.go_ali()
        self.assertIn ('/offers/4325', offer.get_url ())

    #test - click Qatar offer in footer
    def test_goqatar(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        offer = mainPage.go_qatar ()
        self.assertIn ('/offers/27038', offer.get_url ())

    #test - click Add to Chrome extension button in notification in footer
    def test_addext(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        ext = mainPage.go_addext ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn ('/chrome.google.com/webstore/detail', ext.get_url ())

    #test - close add extension notification in footer
    def test_closeextnotification(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible(MainPageLocators.LOGO, 5)
        main = mainPage.close_ext()
        self.assertTrue(main.wait_for_element_is_visible(MainPageLocators.EXT_NOTIFICATION_OPEN,2))


    #test - hover cursor to All stores label in Navigation bar
    def test_howerallstores(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        mainPage.hover(*MainPageLocators.ALL_STORES)
        self.assertTrue(mainPage.check_offersdisplayed())

    #test - hover cursor on All stores - All stores category dropdown
    def test_howerallstores_cat(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        mainPage.hover (*MainPageLocators.ALL_STORES)
        self.assertTrue (mainPage.check_offersdisplayed ())
        mainPage.hover (*MainPageLocators.OFFERS_CAT)
        self.assertTrue(mainPage.check_offerssubdisplayed())


    #test - click category in All stores list
    def test_clickallstores_cat(self):
       mainPage = MainPage (self.driver)
       mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
       mainPage.hover (*MainPageLocators.ALL_STORES)
       self.assertTrue (mainPage.check_offersdisplayed ())
       mainPage.hover (*MainPageLocators.OFFERS_CAT)
#       self.assertTrue (mainPage.check_offerssubdisplayed ())
       offer = mainPage.click_offer()
       self.assertIn ('/offers/', offer.get_url ())

    #test - click All stores label (no login, cleared cookies)
    def test_clickallstores_nologin(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        all = mainPage.click_allstores()
        self.assertTrue(all.check_banner_nologin())

    #test - click How works link in navigation bar
    def test_openhowworks(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        hw = mainPage.click_hw()
        self.assertIn ('/howto/buy', hw.get_url ())

    #test - click Support link in navigation tab
    def test_opensupport(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        support = mainPage.click_support ()
        self.assertIn ('/support', support.get_url ())

    def test_nologinbanner(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue (mainPage.check_bannernologindisplayed ())

    def test_nologinjoin(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue (mainPage.check_joindisplayed ())

    def test_getthebuttonclick(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        gs = mainPage.click_getthebutton()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn ('chrome.google.com/webstore/detail', gs.get_url ())

    def test_bdailyofferclick_nologin(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        sign = mainPage.click_bdo1()
        self.assertTrue(sign.check_signindisplayed)

    def test_seeallclick_nologin(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        all = mainPage.click_seeall()
        self.assertIn ('/join', all.get_url ())

    def test_nologin_bottombanner(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue (mainPage.check_bottombannernologindisplayed ())

    def test_joibbottomdisplayed(self):
        mainPage = MainPage (self.driver)
        mainPage.wait_for_element_is_visible (MainPageLocators.LOGO, 5)
        self.assertTrue (mainPage.check_bottomjoindisplayed ())

    def test_mainbanneroffer_logined(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login ()
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        offer = logineduserpage.click_shopnow()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn('interstitial/offer',offer.get_url())

    def test_usernamedisplayed(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login ()
        self.assertTrue(logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5))

    def test_openuserprofile(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login ()
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        profile = logineduserpage.click_username()
        self.assertIn('/profile',profile.get_url())

    def test_casbackdisplayed(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login ()
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        self.assertTrue(logineduserpage.check_cashbask_displayed())

    def test_topofferdisplayed(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login ()
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        logineduserpage.click_topoffer()
        self.assertIn('/offers/', logineduserpage.get_url())

    def test_bdofferdisplayed(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login ()
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.BD_OFFER, 5)
        logineduserpage.click_bdoffer ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn ('/offer/', logineduserpage.get_url ())

    def test_specialofferdisplayed(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login ()
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.SPECIAL_DEAL, 5)
        logineduserpage.click_specialoffer ()
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.assertIn ('/coupon/', logineduserpage.get_url ())

    def test_allstoresclick(self):
        page = MainPage (self.driver)
        signUpPage = page.click_sign_in_button ()
        signUpPage.wait_for_element_is_visible (LoginPageLocators.SIGNUP_PAGE, 5)
        logineduserpage = signUpPage.login ()
        logineduserpage.wait_for_element_is_visible (LoginedUserPageLocators.USERNAME, 5)
        logineduserpage.click_allstores ()
        self.assertIn ('/all-stores', logineduserpage.get_url ())




    # close driver after the test
    def tearDown(self):
        self.driver.close()