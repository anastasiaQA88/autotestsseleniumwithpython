import unittest
from test.selenium import webdriver
from test.selenium.webdriver.common.keys import Keys


class TestExample(unittest.TestCase):

    #set up webdriver options
    def setUp(self):
        self.driver = webdriver.Chrome(executable_path='C:\selenium\chromedriver_win32\chromedriver.exe')
        self.driver.maximize_window()
        self.driver.get("https://www.python.org/")

    #test example for presentation
    def test_first(self):
        assert "Python" in self.driver.title
        elem = self.driver.find_element_by_id ("id-search-field")
        elem.clear ()
        elem.send_keys ("pycon")
        elem.send_keys (Keys.RETURN)
        assert "No results found." not in self.driver.page_source

    #close driver after the test
    def tearDown(self):
        self.driver.close ()

