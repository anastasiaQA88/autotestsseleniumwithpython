import time

from test.selenium import webdriver
from test.selenium.webdriver.common.keys import Keys
from test.base import Page
from test.locators import *

from test.selenium.webdriver.support.ui import WebDriverWait


# Page objects are written in this module.
# Depends on the page functionality we can have more functions for new classes
from test.selenium.common.exceptions import NoSuchElementException


class MainPage (Page):

    #url = "https://rungrinh.vn/"
    url_dev = "https://dev.rungrinh.vn/"

    def __init__(self, driver):
        self.locator = MainPageLocators
        super ().__init__ (driver)

    def check_page_loaded(self):
        return True if self.find_element (*self.locator.LOGO) else False

    def search_results_not_displayed(self):
        return False if self.find_element (*self.locator.RESULTS) else True


    def click_sign_in_button(self):
        element  = self.find_element (*self.locator.SIGNUP)
        self.driver.execute_script("arguments[0].click();", element)
        return SignUpPage (self.driver)

    def click_join_button(self):
        element = self.find_element (*self.locator.JOIN)
        self.driver.execute_script ("arguments[0].click();", element)
        return JoinPage (self.driver)

    def click_logo(self):
        element = self.find_element (*self.locator.LOGO)
        self.driver.execute_script ("arguments[0].click();", element)
        return MainPage (self.driver)

    def enter_search(self,request):
        element = self.find_element (*self.locator.SEARCH)
        self.driver.execute_script ("arguments[0].click();", element)
        self.find_element (*self.locator.SEARCH_INPUT).send_keys (request)
        time.sleep(2)
        result1 = self.find_element (*self.locator.RESULTS).text
        print(result1)
        return result1

    def click_search(self):
        element = self.find_element (*self.locator.RESULTS)
        self.driver.execute_script ("arguments[0].click();", element)


    def enter_searchempty(self, request):
        element = self.find_element (*self.locator.SEARCH)
        self.driver.execute_script ("arguments[0].click();", element)
        self.find_element (*self.locator.SEARCH_INPUT).send_keys (request)
        time.sleep (2)

    def lang_choose(self):
        element = self.find_element (*self.locator.LANG_CHOOSE)
        self.hover(*self.locator.LANG_CHOOSE)
        self.driver.execute_script ("arguments[0].click();", element)

    def lang_change(self):
        element = self.find_element (*self.locator.LANG_CHOOSE)
        self.hover (*self.locator.LANG_CHOOSE)
        self.driver.execute_script ("arguments[0].click();", element)
        element1 = self.find_element (*self.locator.LANG_LIST)
        self.driver.execute_script ("arguments[0].click();", element1)
        element1 = self.find_element (*self.locator.LANG_CHOOSE).text
        print (element1)
        return element1


    def check_langlistdisplayed(self):
        return True if self.find_element (*self.locator.LANG_LIST) else False

    def follow_facebook(self):
        element = self.find_element (*self.locator.FACEBOOK)
        self.driver.execute_script ("arguments[0].click();", element)
        return FacebookPage(self.driver)

    def follow_twitter(self):
        element = self.find_element (*self.locator.TWITTER)
        self.driver.execute_script ("arguments[0].click();", element)
        return TwitterPage(self.driver)

    def follow_google(self):
        element = self.find_element (*self.locator.GOOGLE)
        self.driver.execute_script ("arguments[0].click();", element)
        return GooglePage(self.driver)

    def go_foradd(self):
        element = self.find_element (*self.locator.FOR_ADV)
        self.driver.execute_script ("arguments[0].click();", element)
        return GoForAddPage (self.driver)

    def go_aboutadd(self):
        element = self.find_element (*self.locator.ABOUT_ADD)
        self.driver.execute_script ("arguments[0].click();", element)
        return GoForAddPage (self.driver)

    def go_howworks(self):
        element = self.find_element (*self.locator.HOW_LINK)
        self.driver.execute_script ("arguments[0].click();", element)
        return HowWorksPage (self.driver)

    def go_terms(self):
        element = self.find_element (*self.locator.TERMS)
        self.driver.execute_script ("arguments[0].click();", element)
        return TermsPage (self.driver)

    def go_privacy(self):
        element = self.find_element (*self.locator.PRIVACY)
        self.driver.execute_script ("arguments[0].click();", element)
        return PrivacyPage (self.driver)

    def go_help(self):
        element = self.find_element (*self.locator.HELP)
        self.driver.execute_script ("arguments[0].click();", element)
        return HelpPage (self.driver)

    def go_ali(self):
        element = self.find_element (*self.locator.ALI)
        self.driver.execute_script ("arguments[0].click();", element)
        return OfferPage (self.driver)

    def go_qatar(self):
        element = self.find_element (*self.locator.QATAR)
        self.driver.execute_script ("arguments[0].click();", element)
        return OfferPage (self.driver)

    def go_addext(self):
        element = self.find_element (*self.locator.ADD_EXT)
        self.driver.execute_script ("arguments[0].click();", element)
        return ExtPage (self.driver)

    def close_ext(self):
        element = self.find_element (*self.locator.CLOSE_EXT)
        self.driver.execute_script ("arguments[0].click();", element)
        return MainPage (self.driver)

    def check_offersdisplayed(self):
        of = self.find_element (*self.locator.OFFERS).text
        return True if self.find_element (*self.locator.OFFERS) and of != '' else False

    def check_offerssubdisplayed(self):
        return True if self.find_element (*self.locator.OFFERS_SUB) else False

    def check_bannernologindisplayed(self):
        return True if self.find_element (*self.locator.BANNER_NOLOGIN) else False

    def check_bottombannernologindisplayed(self):
        return True if self.find_element (*self.locator.BOTTOMBANNER_NOLOGIN) else False

    def check_bottomjoindisplayed(self):
        return True if self.find_element (*self.locator.BOTTOM_JOIN) else False

    def check_joindisplayed(self):
        return True if self.find_element (*self.locator.JOIN_NOLOGIN) else False


    def click_allstores(self):
        element = self.find_element (*self.locator.ALL_STORES)
        self.driver.execute_script ("arguments[0].click();", element)
        return AllStoresPage (self.driver)

    def click_getthebutton(self):
        element = self.find_element (*self.locator.GET_THE_BUTTON)
        self.driver.execute_script ("arguments[0].scrollIntoView();", element)
        self.driver.execute_script ("arguments[0].click();", element)
        return GoogleStorePage (self.driver)

    def click_bdo1(self):
        element = self.find_element (*self.locator.BDO_1)
        self.driver.execute_script ("arguments[0].scrollIntoView();", element)
        self.driver.execute_script ("arguments[0].click();", element)
        return SignUpPage (self.driver)

    def click_seeall(self):
        element = self.find_element (*self.locator.SEE_ALL)
        self.driver.execute_script ("arguments[0].scrollIntoView();", element)
        self.driver.execute_script ("arguments[0].click();", element)
        return JoinPage (self.driver)

    def click_offer(self):
        element = self.find_element (*self.locator.OFFER_GO)
        self.driver.execute_script ("arguments[0].click();", element)
        return OfferPage (self.driver)

    def click_hw(self):
        element = self.find_element (*self.locator.HOW_LINK)
        self.driver.execute_script ("arguments[0].click();", element)
        return HowWorksPage (self.driver)

    def click_support(self):
        element = self.find_element (*self.locator.SUPPORT)
        self.driver.execute_script ("arguments[0].click();", element)
        return SupportPage (self.driver)





class AllStoresPage(Page):

    def __init__(self, driver):
        self.locator = AllStoresLocators
        super ().__init__ (driver)

    def check_banner_nologin(self):
        return True if self.find_element (*self.locator.BANNER) else False



class HomePage (Page):
    pass

class GoogleStorePage (Page):
    pass

class ExtPage(Page):
    pass

class HowWorksPage(Page):
    pass

class TermsPage(Page):
    pass

class PrivacyPage(Page):
    pass

class HelpPage(Page):
    pass

class OfferPage (Page):
    pass

class HowWorksPage(Page):
    pass

class SupportPage(Page):
    pass

class SignUpPage (Page):
    #url = "https://rungrinh.vn/login"
    url_dev = "https://dev.rungrinh.vn/login"


    def __init__(self, driver):
        self.locator = LoginPageLocators
        super ().__init__ (driver)

    def check_page_loaded(self):
        return True if self.find_element (*self.locator.LOGO) else False

    def check_signindisplayed(self):
        return True if self.find_element (*self.locator.SIGNUP_PAGE) else False


    def enter_email(self,email):
        self.find_element (*self.locator.EMAIL).send_keys (email)

    def enter_password(self,password):
        self.find_element (*self.locator.PASSWORD).send_keys (password)

    def forgot_pass(self):
        element = self.find_element (*self.locator.FORGOT_PASSWORD)
        self.driver.execute_script ("arguments[0].click();", element)

    def click_login_button(self):
        element = self.find_element (*self.locator.SUBMIT)
        self.driver.execute_script ("arguments[0].click();", element)

    def enter_forgot_email(self,email):
        self.find_element (*self.locator.FORGOT_PASSWORD_EMAIL).send_keys (email)

    def restore_pass_click(self):
        element = self.find_element (*self.locator.RESTORE_PASS_BUTTON)
        self.driver.execute_script ("arguments[0].click();", element)

    def restore_ok_check(self):
        return True if self.find_element (*self.locator.OK_NOTIFICATION) else False

    def login(self):
        #self.enter_email ("podgornova.a@gmail.com")
        #self.enter_password ("Testpass123")
        self.enter_email ("yade@twocowmail.net")
        self.enter_password ("Qwerty123456")
        self.click_login_button ()
        return LoginedUserPage(self.driver)

    def login_incorrect (self):
        self.enter_email ("blabla@gmail.com")
        self.enter_password ("Testpass123")
        self.click_login_button ()
        return LoginedUserPage (self.driver)

    def forgot_password(self):
        self.wait_for_element_is_visible(LoginPageLocators.FORGOT_PASSWORD,5)
        self.forgot_pass()
        self.wait_for_element_is_visible(LoginPageLocators.FP_WINDOW)
        return True if self.find_element (*self.locator.FP_WINDOW) else False

    def go_to_join(self):
        element = self.find_element (*self.locator.GO_TOJOIN)
        self.driver.execute_script ("arguments[0].click();", element)
        return JoinPage(self.driver)

    def close_signin(self):
        element = self.find_element (*self.locator.X_CLOSE)
        self.driver.execute_script ("arguments[0].click();", element)
        return MainPage(self.driver)

    def click_logo(self):
        element = self.find_element (*self.locator.LOGO)
        self.driver.execute_script ("arguments[0].click();", element)
        return MainPage (self.driver)

    def signin_fb(self):
        element = self.find_element (*self.locator.FACEBOOK_BUTTON)
        self.driver.execute_script ("arguments[0].click();", element)
        return FacebookPopup(self.driver)

    def join_fb(self):
        element = self.find_element (*self.locator.JOIN_FACEBOOK_BUTTON)
        self.driver.execute_script ("arguments[0].click();", element)
        return FacebookPopup(self.driver)

    def signin_google(self):
        element = self.find_element (*self.locator.GOOGLE_BUTTON)
        self.driver.execute_script ("arguments[0].click();", element)
        return GooglePopup (self.driver)


    def check_fb(self):
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.wait_for_element_is_visible (FacebookPopupLocators.FB_WINDOW)
        return True if self.find_element (*self.locator.FB_WINDOW) else False
        self.driver.switch_to.window (self.driver.window_handles[0])

    def check_google(self):
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.wait_for_element_is_visible (GooglePopupLocators.GOOGLE_WINDOW)
        return True if self.find_element (*self.locator.GOOGLE_WINDOW) else False
        self.driver.switch_to.window (self.driver.window_handles[0])

    def join_google(self):
        element = self.find_element (*self.locator.JOIN_GOOGLE_BUTTON)
        self.driver.execute_script ("arguments[0].click();", element)
        return GooglePopup(self.driver)



class JoinPage(Page):
    def __init__(self, driver):
        self.locator = JoinPageLocators
        super ().__init__ (driver)

    def goto_signin(self):
        element = self.find_element (*self.locator.GOTO_SIGNIN)
        self.driver.execute_script ("arguments[0].click();", element)
        return SignUpPage (self.driver)


    def join_fb(self):
        element = self.find_element (*self.locator.JOIN_FACEBOOK_BUTTON)
        self.driver.execute_script ("arguments[0].click();", element)
        return FacebookPopup(self.driver)

    def join_google(self):
        element = self.find_element (*self.locator.JOIN_GOOGLE_BUTTON)
        self.driver.execute_script ("arguments[0].click();", element)
        return GooglePopup(self.driver)

    def check_page_loaded(self):
        return True if self.find_element (*self.locator.LOGO) else False

    def enter_jemail(self,email):
        self.find_element (*self.locator.EMAIL).send_keys (email)

    def enter_jpassword(self,password):
        self.find_element (*self.locator.PASSWORD).send_keys (password)

    def click_jlogin_button(self):
        element = self.find_element (*self.locator.SUBMIT)
        self.driver.execute_script ("arguments[0].click();", element)

    def jlogin_taken (self):
        self.enter_jemail ("podgornova.a@gmail.com")
        self.enter_jpassword ("Testpass123")
        self.click_jlogin_button ()
        return JoinPage (self.driver)

    def jlogin_empty (self):
        self.enter_jemail ("")
        self.enter_jpassword ("")
        self.click_jlogin_button ()
        return JoinPage (self.driver)

    def check_jlogin_error_presented(self):
        return True if self.find_element(*self.locator.EMAIL_TAKEN) else False

    def check_jlogin_errorempty_presented(self):
        return True if self.find_element(*self.locator.ERROR_EMPTY) else False

    def close_join(self):
        element = self.find_element (*self.locator.CLOSE_JOIN)
        self.driver.execute_script ("arguments[0].click();", element)
        return MainPage(self.driver)

    def click_logo(self):
        element = self.find_element (*self.locator.LOGO)
        self.driver.execute_script ("arguments[0].click();", element)
        return MainPage (self.driver)


class LoginedUserPage(Page):

    def __init__(self, driver):
        self.locator = LoginedUserPageLocators
        super ().__init__ (driver)

    def check_user_logined(self):
        return True if self.find_element(*self.locator.USERNAME) else False

    def check_cashbask_displayed(self):
        return True if self.find_element (*self.locator.USER_CASHBACK) else False

    def click_username(self):
        element = self.find_element (*self.locator.USERNAME)
        self.driver.execute_script ("arguments[0].click();", element)
        return UserProfilePage (self.driver)

    def click_topoffer(self):
        element = self.find_element (*self.locator.TOP_OFFER)
        self.driver.execute_script ("arguments[0].click();", element)

    def click_bdoffer(self):
        element = self.find_element (*self.locator.BD_OFFER_CLICK)
        self.driver.execute_script ("arguments[0].click();", element)

    def click_specialoffer(self):
        element = self.find_element (*self.locator.SPECIAL_DEAL_CLICK)
        self.driver.execute_script ("arguments[0].click();", element)

    def click_allstores(self):
        element = self.find_element (*self.locator.ALL_STORES)
        self.driver.execute_script ("arguments[0].click();", element)





    def check_login_error_presented(self):
        return True if self.find_element(*self.locator.LOGIN_ERROR) else False

    def click_shopnow(self):
        element = self.find_element (*self.locator.SHOPNOW_BANNER)
        self.driver.execute_script ("arguments[0].click();", element)
        return OfferPage (self.driver)

class FacebookPopup(Page):
    def __init__(self, driver):
        self.locator = FacebookPopupLocators
        super ().__init__ (driver)

    def enter_fb_email(self,email):
        self.find_element (*self.locator.EMAIl_FB).send_keys (email)

    def enter_fb_pass(self,password):
        self.find_element (*self.locator.PASS_FB).send_keys (password)

    def fb_submit(self):
        element = self.find_element (*self.locator.LOGIN_FB)
        self.driver.execute_script ("arguments[0].click();", element)

    def login_fb_correct(self):
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.enter_fb_email("podgornova.a@gmail.com")
        self.enter_fb_pass ("Myfacebookpass123")
        self.fb_submit()
        self.driver.switch_to.window (self.driver.window_handles[0])
        return LoginedUserPage (self.driver)


    def login_fb_incorrect(self):
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.enter_fb_email("blablabla@gmail.com")
        self.enter_fb_pass ("123")
        self.fb_submit()
        return FacebookPopup (self.driver)

    def check_fb_error_presented(self):
        self.driver.switch_to.window (self.driver.window_handles[1])
        return True if self.find_element (*self.locator.ERROR) else False
        self.driver.switch_to.window (self.driver.window_handles[0])

    def check_fb(self):
        self.driver.switch_to.window (self.driver.window_handles[1])
        self.wait_for_element_is_visible (FacebookPopupLocators.FB_WINDOW)
        return True if self.find_element (*self.locator.FB_WINDOW) else False
        self.driver.switch_to.window (self.driver.window_handles[0])


    def close_fb(self):
        self.driver.switch_to.window (self.driver.window_handles[1])
        element = self.find_element (*self.locator.CLOSE)
        self.driver.execute_script ("arguments[0].click();", element)
        self.driver.switch_to.window (self.driver.window_handles[0])
        return SignUpPage(self.driver)

class GooglePopup(Page):
    def __init__(self, driver):
        self.locator = GooglePopupLocators
        super ().__init__ (driver)

    def enter_go_email(self,email):
            self.find_element (*self.locator.EMAIL_GO).send_keys (email)
            self.find_element (*self.locator.EMAIL_GO).send_keys(Keys.ENTER)


    def enter_go_pass(self,password):
            self.find_element (*self.locator.PASS_GO).send_keys (password)
            self.find_element (*self.locator.PASS_GO).send_keys (Keys.ENTER)

    def change_acc(self):
            element = self.find_element (*self.locator.CHANGE_ACC)
            self.driver.execute_script ("arguments[0].click();", element)


    def login_go_correct(self):
            self.driver.switch_to.window (self.driver.window_handles[1])
            #self.change_acc()
            self.enter_go_email ("podgornova.a@gmail.com")
            time.sleep(2)
            self.driver.switch_to.window (self.driver.window_handles[0])
            self.driver.switch_to.window (self.driver.window_handles[1])
            self.enter_go_pass ("Mygooglepass123")
            self.driver.switch_to.window (self.driver.window_handles[0])
            return LoginedUserPage (self.driver)

    def login_go_incorrect(self):
            self.driver.switch_to.window (self.driver.window_handles[1])
            #self.change_acc()
            self.enter_go_email ("blablablaцццц@gmail.com")
            time.sleep(2)
            self.driver.switch_to.window (self.driver.window_handles[0])
            return GooglePopup (self.driver)

    def check_glogin_error_presented(self):
        self.driver.switch_to.window (self.driver.window_handles[1])
        return True if self.find_element (*self.locator.ERROR) else False
        self.driver.switch_to.window (self.driver.window_handles[0])



    def close_google(self):
        self.driver.switch_to.window (self.driver.window_handles[0])
        element = self.find_element (*self.locator.LOGO)
        self.driver.execute_script ("arguments[0].click();", element)
        return SignUpPage(self.driver)


class FacebookPage(Page):
    pass

class UserProfilePage(Page):
    pass

class TwitterPage(Page):
    pass

class GooglePage(Page):
    pass

class GoForAddPage(Page):
    pass













